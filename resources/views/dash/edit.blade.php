@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
    <h1>editar usuario {{$user->name}}</h1>
@stop

@section('content')
<div class="w-50 m-auto">
<form action="/dash/{{$user->id}}" method="POST">    
   @csrf
   @method('PUT')
  <div class="mb-3">
    <label for="" class="form-label">name</label>
    <input id="name" name="name" type="text" class="form-control" value="{{$user->name}}">    
  </div>
  <div class="mb-3">
    <label for="email" class="form-label">email</label>
    <input id="email" name="email" type="email" class="form-control" value="{{$user->email}}">
  </div>
  <div class="mb-3">
    <label for="password" class="form-label">password</label>
    <input id="password" name="password" type="password" class="form-control" >
  </div>

  <a href="/dash" class="btn btn-secondary">Cancelar</a>
  <button type="submit" class="btn btn-primary">Guardar</button>
</form>
</div>
@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('js')
    <script> console.log('Hi!'); </script>
@stop