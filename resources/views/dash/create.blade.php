@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
    <h1>crear usuario</h1>
@stop

@section('content')
<div class="w-50 m-auto">

    <form action="{{ route ('dash.store')}}" method="POST">    
    @csrf
    <div class="mb-3">
        <label for="" class="form-label">name</label>
        <input id="name" name="name" type="text" class="form-control" >    
    </div>
    <div class="mb-3">
        <label for="email" class="form-label">email</label>
        <input id="email" name="email" type="email" class="form-control" >
    </div>
    <div class="mb-3">
        <label for="password" class="form-label">password</label>
        <input id="password" name="password" type="password" class="form-control" >
    </div>

    <a href="/dash" class="btn btn-secondary">Cancelar</a>
    <button type="submit" class="btn btn-primary">Guardar</button>
    </form>
<div>
@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('js')
    <script> console.log('Hi!'); </script>
@stop