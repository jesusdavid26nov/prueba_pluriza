@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
    <h1>Operacion</h1>
@stop

@section('content')
<div class="w-50 m-auto">

    <div >    
        <div class="mb-3">
            <label for="numberOne" class="form-label">numero uno</label>
            <input id="numberOne" name="numberOne" type="number" class="form-control" >    
        </div>
        <div class="mb-3">
            <label for="numberTwo" class="form-label">numero dos</label>
            <input id="numberTwo" name="numberTwo" type="number" class="form-control" >
        </div>
        <div class="mb-3">
                <select id="operator" class="form-control" aria-label="Default select example">
                    <option selected>seleccione el operador</option>
                    <option value="+">+</option>
                    <option value="-">-</option>
                    <option value="*">*</option>
                    <option value="/">/</option>
                    <option value="<"><</option>
                    <option value=">">></option>
                    <option value="=">=</option>
                </select>
        </div>
        <div class="">
            <span>Resultado</span>
            <p id="result"></p>
        </div>
        <a href="/dash" class="btn btn-secondary">Cancelar</a>
            <button class="btn btn-primary" onclick={operatorGet()}>Guardar</button>
        </div>
    <div>
@stop

@section ('css')
    <link rel="stylesheet" href="/css/admin_custom.css" >
@stop

@section('js')
    <script> 

    async  function operatorGet () {
        const data = {
            numberOne: document.getElementById('numberOne').value,
            numberTwo: document.getElementById('numberTwo').value,
            operator: document.getElementById('operator').value

        }
        const result = await postData('http://127.0.0.1:8000/api/operator/result', data)
        const resultId= document.getElementById('result');
        resultId.innerText = result;

    }
     async function postData(url = '', data = {}) {
        const response = await fetch(url, {
                method: 'POST',
                headers: {
                'Content-Type': 'application/json'
                },
                body: JSON.stringify(data) 
            });
        return response.json(); 
        }

    </script>
@stop