<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class operationController extends Controller
{
    public function index()
    {
        return view('operation.index');
    }
    
    public function operation(Request $request)
    {
        switch ($request->operator) {
            case '+':
                $result = $request->numberOne + $request->numberTwo; 
                return response()->json($result);
            case '-':
                $result = $request->numberOne - $request->numberTwo; 
                return response()->json($result);
            
            case '/':
                $result = $request->numberOne / $request->numberTwo; 
                return response()->json($result);
            break;
            case '*':
                $result = $request->numberOne * $request->numberTwo; 
                return response()->json($result);
            case '<':
                if ($request->numberOne < $request->numberTwo) {
                    return response()->json("es menor");
                }else{
                    return response()->json("no es menor");
                }
               
            case '>':
                if ($request->numberOne > $request->numberTwo) {
                    return response()->json("es mayor");
                }else{
                    return response()->json("no es mayor");
                }
            case '=':
                if ($request->numberOne === $request->numberTwo) {
                    return response()->json("es igual");
                }else{
                    return response()->json("no es igual");
                }
            default:
                return response()->json("ningun valor");
        }
    }

}
